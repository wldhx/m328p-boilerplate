#################
m328p-boilerplate
#################
A starting point for AVR (particularly Atmega M328P, not tied to it though) development.

********
Features
********
Makefile
========
Just type ``make`` to get your code compiled! Having your MCU programmed with it is just a ``sudo make install`` away!

Commands: ``make``
----------------
========== ====================================================
(none)      @compile
compile     compile the code (lots of warnings and -Os enabled)
clean       remove object files
install     flash the program onto the MCU
uninstall   clean the MCU's flash memory
========== ====================================================

Default compilation options
---------------------------
.. code::

   -mmcu=atmega328p # target platform
   -std=c99 # c standard to compile against
   -Wall -Wextra -Wfloat-equal -Wundef -Wshadow -Wpointer-arith -Wcast-align -Wstrict-prototypes -Wmissing-prototypes -Wstrict-overflow=5 -Wwrite-strings -Waggregate-return -Wcast-qual -Wswitch-default -Wswitch-enum -Wconversion -Wunreachable-code -Wno-unused-result -Werror-implicit-function-declaration -pedantic # warnings
   -ftrapv # code generation options
   -Os # optimisation for size (O2 modification)

Default programmer options
--------------------------
.. code::

   -p m328p # destination platform
   -c usbasp-clone # programmer device
